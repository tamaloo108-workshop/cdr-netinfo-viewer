using NPP.REIM.Services.Generics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NPP.REIM.Initializations
{
    public class FirstLoad : MonoBehaviour
    {
        SceneLoader _loader => Services.ServiceLocator.Current.Get<SceneLoader>();

        private void Start()
        {
            _loader.LoadScene("Main Menu");
        }

    }
}