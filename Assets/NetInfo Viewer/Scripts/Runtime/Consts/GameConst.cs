using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConst
{
    public const string CHARACTER_DEFAULT = "Default";

    public const string SCENE_INITIALIZE_DEFAULT = "Initialize [0]";
    public const string SCENE_MAINMENU_DEFAULT = "Main Menu [1]";
    public const string SCENE_GAME_DEFAULT = "Game";
    public const string SCENE_DATA_DISPLAYS = "Data Collection";

    public const byte RE_SPAWN_MAP = 10;
    public const byte RE_SPAWN_CHAR = 11;
    public const byte RE_LOCAL_SEND_INPUT = 12;
    public const byte RE_SYNC_TIMER = 13;
    public const byte RE_SPAWN_TRAVELER = 14;
    public const byte RE_DRIVER_DELIVERY = 15;
    public const byte RE_DESPAWN_OBJECT = 16;

    public const string RE_SPAWN_MAP_STRING = "SpawnMap";
    public const string RE_SPAWN_CHAR_STRING = "SpawnChar";
    public const string RE_LOCAL_SEND_INPUT_STRING = "SendInput";
    public const string RE_SYNC_TIMER_STRING = "SyncTime";
    public const string RE_SPAWN_TRAVELER_STRING = "SpawnTraveler";
    public const string RE_DRIVER_DELIVERY_STRING = "DriverDelivery";
    public const string RE_DESPAWN_OBJECT_STRING = "DespawnObject";

    public const string PROP_PLAYER_INITIALIZED = "ClientDriverSpawned";
    public const string PROP_MAP_INITIALIZED = "MasterClientMapInitialized";
    public const string PROP_MSG_METHOD = "ClientMsgMethod";
    public static readonly string PROP_MSG_RPC = "Remote Procedure Call";
    public static readonly string PROP_MSG_RE = "Raise Event";

    public const string RPC_MSG_SEND_INPUT = "RPC_SendInput";
    public const string RPC_MSG_SPAWN_MAP = "RPC_SetLoadedMap";
    public const string RPC_MSG_SPAWN_PLAYER = "RPC_LoadCharacter";
    public const string RPC_MSG_SYNC_TIMER = "RPC_SyncTime";
    public const string RPC_MSG_SPAWN_TRAVELER = "RPC_SpawnTraveler";
    public const string RPC_MSG_DRIVER_DELIVERY = "RPC_DriverDelivery";
    public const string RPC_MSG_DESPAWN_OBJECT = "RPC_DespawnObject";

    public const float GAME_TIME_DECREMENT = 1f;
    public const int UDP_HEADER_SIZE = 8;

    public const string OBJ_DESPAWN_TRAVELER = "TR";
    public const string OBJ_DESPAWN_DRIVER = "DR";
    public const string OBJ_DESPAWN_PBOX = "PB";

    public static string RaiseEventCodeToString(byte code)
    {
        return code switch
        {
            RE_SPAWN_CHAR => RE_SPAWN_CHAR_STRING,
            RE_SPAWN_MAP => RE_SPAWN_MAP_STRING,
            RE_LOCAL_SEND_INPUT => RE_LOCAL_SEND_INPUT_STRING,
            RE_SYNC_TIMER => RE_SYNC_TIMER_STRING,
            RE_SPAWN_TRAVELER => RE_SPAWN_TRAVELER_STRING,
            RE_DRIVER_DELIVERY => RE_DRIVER_DELIVERY_STRING,
            RE_DESPAWN_OBJECT => RE_DESPAWN_OBJECT_STRING,
            _ => "Default",
        };
    }
}
