﻿using NPP.REIM.Services;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace NPP.REIM.DataLogging
{
    public class WebFtpServices : IServices
    {
        private bool _uploadComplete = false;

        public bool UploadComplete => _uploadComplete;

        private string _username;
        private string _password;
        private string _server;
        private string _remoteDir;

        public void SetCredentials(string username, string password, string server, string remotePath)
        {
            this._username = username;
            this._password = password;
            this._server = server;
            this._remoteDir = remotePath;
        }

        public async Task<string[]> GetDirectoryContents()
        {
            string[] data = null;
            try
            {
                var address = new System.Uri("ftp://" + _server + "/" + _remoteDir + "/");
                UnityEngine.Debug.Log(address);

                var req = FtpWebRequest.Create(address) as FtpWebRequest;

                req.Credentials = new NetworkCredential(_username, _password);
                req.UseBinary = true;
                req.KeepAlive = true;
                req.UsePassive = true;

                req.Method = WebRequestMethods.Ftp.ListDirectory;
                var response = await req.GetResponseAsync() as FtpWebResponse;
                var stream = response.GetResponseStream();

                StreamReader reader = new StreamReader(stream);
                string dirRaw = null;
                try
                {
                    while (reader.Peek() != -1)
                    {
                        dirRaw += await reader.ReadLineAsync() + "|";
                    }
                }
                catch (System.Exception e)
                {
                    UnityEngine.Debug.Log("failed to get content, " + e.Message);
                    return data;
                }

                reader.Close();
                stream.Close();
                response.Close();

                try
                {
                    string[] dirList = dirRaw.Split("|".ToCharArray());
                    data = dirList;
                }
                catch (System.Exception e)
                {
                    UnityEngine.Debug.Log("failed to parse content, " + e.Message);
                    return data;
                }
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.Log("fatal error, " + e.Message);
                return data;
            }
            return data;
        }

        #region Not Required
        //public void Upload(string fileName, string server, string username, string password, string remotePath, string localPath)
        //{
        //    _uploadComplete = false;
        //    var file = new FileInfo(fileName);
        //    var address = new System.Uri("ftp://" + server + "/" + Path.Combine(remotePath, file.Name));
        //    var req = FtpWebRequest.Create(address) as FtpWebRequest;

        //    req.Credentials = new NetworkCredential(username, password);
        //    req.KeepAlive = true;
        //    req.UseBinary = true;
        //    req.UsePassive = true;
        //    req.Method = WebRequestMethods.Ftp.UploadFile;

        //    FileInfo _localFile = new FileInfo(localPath);

        //    var stream = req.GetRequestStream();
        //    FileStream fs = _localFile.OpenRead();
        //    byte[] buffer = new byte[2048];
        //    int byteSent = fs.Read(buffer, 0, 2048);

        //    try
        //    {
        //        while (byteSent != 0)
        //        {
        //            stream.Write(buffer, 0, byteSent);
        //            byteSent = fs.Read(buffer, 0, 2048);
        //        }
        //    }
        //    catch (System.Exception e)
        //    {
        //        UnityEngine.Debug.Log("upload unsuccessful. error :" + e.Message);
        //        return;
        //    }

        //    fs.Close();
        //    stream.Close();

        //    UnityEngine.Debug.Log("upload successful.");
        //    _uploadComplete = true;
        //}
        #endregion

        public async Task<byte[]> Download(string fileName)
        {
            var file = new FileInfo(fileName);
            var address = new System.Uri("ftp://" + _server + "/" + Path.Combine(_remoteDir, file.Name));
            var req = FtpWebRequest.Create(address) as FtpWebRequest;

            req.Credentials = new NetworkCredential(_username, _password);
            req.KeepAlive = true;
            req.UsePassive = true;
            req.UseBinary = true;

            req.Method = WebRequestMethods.Ftp.DownloadFile;
            var response = await req.GetResponseAsync() as FtpWebResponse;
            var stream = response.GetResponseStream();
            byte[] buffer = new byte[2048];
            int byteRead = stream.Read(buffer, 0, 2048);

            using (var ms = new MemoryStream())
            {
                while (byteRead > 0)
                {
                    await ms.WriteAsync(buffer, 0, byteRead);
                    byteRead = await stream.ReadAsync(buffer, 0, 2048);
                }

                ms.Close();
                response.Close();
                stream.Close();

                return ms.ToArray();
            }
        }
    }
}