﻿using System.Threading.Tasks;

namespace NPP.REIM.DataLogging
{
    public class CsvHelper
    {
        public CsvHelper()
        {
            _headerSet = false;
        }

        private string _csv;

        public string CSV => _csv;

        private bool _headerSet = false;
        private int _headerLength = 0;

        public void SetHeader(params string[] headerName)
        {
            _headerSet = true;
            _headerLength = headerName.Length;
            for (int i = 0; i < headerName.Length; i++)
            {
                _csv += $"{headerName[i]}{IsEndOfRow(i, headerName)}";
            }
        }

        public void AddData(params string[] data)
        {
            if (!_headerSet) return;
            for (int i = 0; i < data.Length; i++)
            {
                _csv += $"{data[i]}{IsNextLine(i, _headerLength)}";
            }
        }

        public async void AddDataAsync(params string[] data)
        {
            if (!_headerSet) return;
            for (int i = 0; i < data.Length; i++)
            {
                _csv += $"{data[i]}{IsNextLine(i, _headerLength)}";
                await Task.Delay(10);
            }
            await Task.Delay(100);
        }

        private string IsEndOfRow(int index, string[] data)
        {
            return index < data.Length - 1 ? "," : "\n";
        }
        private string IsNextLine(int index, int length)
        {
            return (index + 1) % length != 0 ? "," : "\n";
        }


    }
}