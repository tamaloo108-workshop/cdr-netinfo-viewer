using NPP.REIM.Services.Generics;
using System.Collections.Generic;

namespace NPP.REIM.DataLogging
{

    [System.Serializable]
    public struct NetFunctionInfo : ISerializable
    {
        public string PlayerId;
        public string NetFuncName;
        public string CommunicationType;
        public int DataSize;
        public int DatagramSize;
        public string TimeSent;
        public string TimeReceived;
        public System.TimeSpan Diff;
        public double DiffInMiliseconds;

        public NetFunctionInfo(string playerId, string netFuncName, string commType, int dataSize, int datagramSize, string timeSent, string timeReceived, System.TimeSpan diff, double diffInMiliseconds)
        {
            PlayerId = playerId;
            NetFuncName = netFuncName;
            CommunicationType = commType;
            DataSize = dataSize;
            DatagramSize = datagramSize;
            TimeSent = timeSent;
            TimeReceived = timeReceived;
            Diff = diff;
            DiffInMiliseconds = diffInMiliseconds;
        }
    }

    [System.Serializable]
    public struct GeneralInfo : ISerializable
    {
        public string MatchId;
        public bool IsMasterClient;
        public string IpAddress;

        public GeneralInfo(string matchId, bool isMasterClient, string ip)
        {
            MatchId = matchId;
            IsMasterClient = isMasterClient;
            IpAddress = ip;
        }
    }

    [System.Serializable]
    public class NetworkDataLog : ISerializable
    {
        public GeneralInfo RoomInfo;
        public List<NetFunctionInfo> FunctionInfo;

        public NetworkDataLog()
        {
            FunctionInfo = new List<NetFunctionInfo>();
        }
    }
}