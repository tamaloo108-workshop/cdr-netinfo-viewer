using MarkupAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NPP.REIM.Configs
{
    [CreateAssetMenu(fileName = "CDR NetInfo Viewer", menuName = "NPP/Configs/Create New Game Config")]
    public class ViewerConfigData : ScriptableObject
    {
        [Header("FTP Configuration")]
        public string Username;
        [PasswordField] public string Password;
        [ReadOnly] public string Server;
        [ReadOnly] public string Port;
        public string FtpDirectory;

    }
}