﻿using UnityEngine;

namespace NPP.REIM.Configs
{
    [System.AttributeUsage(System.AttributeTargets.Field, Inherited = true)]
    public class PasswordFieldAttribute : PropertyAttribute
    {
    }
}