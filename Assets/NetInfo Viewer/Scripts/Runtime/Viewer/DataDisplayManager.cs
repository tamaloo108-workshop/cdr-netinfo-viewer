using NPP.REIM.DataLogging;
using NPP.REIM.Services.Generics;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;
using UnityEngine.Serialization;
using TMPro;
using STVR.SimpleDelayer;
using SimpleFileBrowser;
using Cysharp.Threading.Tasks;

namespace NPP.REIM.Viewers
{
    public class CsvElement
    {
        public string MatchId;
        public string IpAddress;
        public string PlayerId;
        public string FunctionName;
        public string CommType;
        public string DataSize;
        public string DatagramSize;
        public string Sent;
        public string Received;
        public string TimeMiliseconds;

        public CsvElement() { }

        public string Write()
        {
            return $"{MatchId},{IpAddress},{PlayerId},{FunctionName},{CommType},{DataSize},{DatagramSize},{Sent},{Received},{TimeMiliseconds}";
        }

        public string[] PackContent()
        {
            return new string[]
            {
                MatchId,
                IpAddress,
                PlayerId,
                FunctionName,
                CommType,
                DataSize,
                DatagramSize,
                Sent,
                Received,
                TimeMiliseconds
            };
        }
    }

    public class DataDisplayManager : MonoBehaviour
    {
        JSONSerializer _jsonSerializer => Services.ServiceLocator.Current.Get<JSONSerializer>();
        WebFtpServices _ftpServices => Services.ServiceLocator.Current.Get<WebFtpServices>();

        [SerializeField, FormerlySerializedAs("_getDataButton")]
        private Button _downloadButton;
        [SerializeField]
        private TextMeshProUGUI _statusTexts;
        [SerializeField]
        private Image _progressBarFiller;
        [SerializeField]
        private Transform _progressBarParent;
        [SerializeField]
        private Transform _statusBarParent;
        [SerializeField]
        private TMP_InputField _maxFuncCallInput;
        [SerializeField]
        private TMP_InputField _contentLimitCount;

        private List<CsvElement> _csvElements;
        private int _contentLength = 0;
        private int _maxFunctionPerNetInfo = 0;
        private int _byteWritten = 0;
        private int _byteLeft = 0;
        private int _contentLimit = 0;

        private float _sendInputPercent = 50;
        private float _driverDeliveryPercent = 40;
        private float _spawnMapPercent = 10;

        private int _maxSendInputCount = 0;
        private int _maxDriverDeliveryCount = 0;
        private int _maxSpawnMapCount = 0;

        private int _sendInputCount = 0;
        private int _driverDeliveryCount = 0;
        private int _spawnMapCount = 0;

        private bool _finishDownload = false;
        private bool _finishCalc = false;
        CsvHelper _csvHelper;

        // Start is called before the first frame update
        void Start()
        {
            _ftpServices.SetCredentials(GlobalServices.GameConfig.Username,
                                        GlobalServices.GameConfig.Password,
                                        GlobalServices.GameConfig.Server,
                                        GlobalServices.GameConfig.FtpDirectory);


            _csvHelper = new CsvHelper();
            _csvElements = new List<CsvElement>();
            _progressBarParent.gameObject.SetActive(false);
            _statusBarParent.gameObject.SetActive(false);

            UpdateMaxFunctionCall((14).ToString());

            _maxFuncCallInput.onValueChanged.AddListener((string t) => UpdateMaxFunctionCall(t));
            _contentLimitCount.onValueChanged.AddListener((string t) => UpdateContentLimit(t));
            _downloadButton.onClick.AddListener(() => TryGetData());
        }

        private async void TryGetData()
        {
            #region Initialization Value
            _downloadButton.interactable = false;
            _maxFuncCallInput.interactable = false;
            _contentLimitCount.interactable = false;
            _finishDownload = false;
            _finishCalc = false;

            _statusBarParent.gameObject.SetActive(true);
            _progressBarFiller.fillAmount = 0f;

            string[] content = await _ftpServices.GetDirectoryContents();
            _contentLimit = int.Parse(_contentLimitCount.text)
                            > 0 ? Mathf.Clamp(int.Parse(_contentLimitCount.text), 0, content.Length) : content.Length;

            _statusTexts.text = $"calculating content length..";

            _csvHelper.SetHeader(
                     "MatchId",
                     "Ip Address",
                     "Player ID",
                     "Function Name",
                     "Communication Type",
                     "Data Size",
                     "Datagram Size",
                     "Sent",
                     "Received",
                     "Execution in Miliseconds");

            #endregion

            #region Local Functions
            float GetDownloadDelayDuration()
            {
                if (!_finishDownload)
                    return 1f;

                return 2f;
            }

            float CheckDuration()
            {
                if (!_finishCalc)
                    return 1f;

                return 2f;
            }

            void OnSuccessSaveFile(string[] paths)
            {
                ConfigurePaths(paths, out string fileName, out string dir);

                InitializeWriteToFile(dir, fileName);

                DownloadFileProgress();
            }

            void DownloadFileProgress()
            {
                Delay.CreateCount(GetDownloadDelayDuration(), () =>
                {
                    _statusTexts.text = $"Downloading {_byteWritten}/{_csvHelper.CSV.Length} B";
                    float _fillNormalized = GlobalServices.ConvertRatio(_byteWritten, 0f, _csvHelper.CSV.Length);
                    _progressBarFiller.fillAmount = _fillNormalized;
                }, () =>
                {

                    if (!_finishDownload)
                    {
                        DownloadFileProgress();
                    }
                    else
                    {
                        _statusTexts.text = $"Download completed.";
                        _downloadButton.interactable = true;
                        _maxFuncCallInput.interactable = true;
                        _contentLimitCount.interactable = true;
                        _progressBarParent.gameObject.SetActive(false);
                        _statusBarParent.gameObject.SetActive(false);
                    }
                },
                .015f);
            }

            void OnDownloadAborted()
            {
                _statusTexts.text = $"Download aborted.";
                _downloadButton.interactable = true;
                _maxFuncCallInput.interactable = true;
                _contentLimitCount.interactable = true;
                _progressBarParent.gameObject.SetActive(false);
                _statusBarParent.gameObject.SetActive(false);
            }

            void PackingIntoCsv()
            {
                int count = 0;
                Delay.CreateCount(_csvElements.Count, () =>
                {
                    _csvHelper.AddData(_csvElements[count].PackContent());
                    _statusTexts.text = $"Packing.. {count}/{_csvElements.Count - 1}";
                    count++;
                }, () =>
                {
                    _statusTexts.text = $"Downloading content..";

                    _progressBarParent.gameObject.SetActive(true);

                    FileBrowser.ShowSaveDialog(OnSuccessSaveFile, OnDownloadAborted, FileBrowser.PickMode.Files, false, null, "netInfo-data.csv");
                },
                .015f);
            }

            void UpdateStatusBar()
            {
                Delay.CreateCount(CheckDuration(), () =>
                {
                    _statusTexts.text = $"Calculating content : {_contentLength}";
                }, () =>
                {
                    if (!_finishCalc)
                        UpdateStatusBar();
                    else
                    {
                        _statusTexts.text = $"Final calculated content : {_contentLength}";
                        Delay.CreateCount(1f, () =>
                        {
                            _statusTexts.text = $"Packing content into csv..";
                            PackingIntoCsv();
                        });
                    }
                }, .0135f);
            }
            #endregion

            #region Processing NetInfo
            UpdateStatusBar();

            for (int i = 0; i < _contentLimit; i++)
            {
                if (content[i].Contains("NetInfo"))
                {
                    byte[] item = await _ftpServices.Download(content[i]);
                    NetworkDataLog logFile = _jsonSerializer.Parse<NetworkDataLog>(item);

                    for (int j = 0; j < logFile.FunctionInfo.Count; j++)
                    {
                        if (logFile.FunctionInfo[j].DiffInMiliseconds < 0f) continue;

                        AddValidNetFunctions(logFile, j);
                    }

                    _spawnMapCount = 0;
                    _driverDeliveryCount = 0;
                    _sendInputCount = 0;

                }
                else
                    continue;
            }

            _finishCalc = true;
            #endregion
        }

        private void UpdateContentLimit(string t)
        {
            _contentLimitCount.text = t;
        }

        private int CalculatePercentage(float driverDeliveryPercent, float maxFunctionPerNetInfo)
        {
            return Mathf.FloorToInt((driverDeliveryPercent / 100f) * (maxFunctionPerNetInfo + 1));
        }

        private void UpdateMaxFunctionCall(string t)
        {
            _maxFuncCallInput.text = t;
            _maxFunctionPerNetInfo = int.Parse(_maxFuncCallInput.text);
            Debug.Log(_maxFunctionPerNetInfo);
            _maxDriverDeliveryCount = CalculatePercentage(_driverDeliveryPercent, _maxFunctionPerNetInfo);
            _maxSendInputCount = CalculatePercentage(_sendInputPercent, _maxFunctionPerNetInfo);
            _maxSpawnMapCount = CalculatePercentage(_spawnMapPercent, _maxFunctionPerNetInfo);
        }

        private void ConfigurePaths(string[] paths, out string fileName, out string dir)
        {
            string[] dirArr = paths[0].Split("\\".ToCharArray());
            fileName = paths[paths.Length - 1];
            dir = "";
            for (int i = 0; i < dirArr.Length - 1; i++)
            {
                dir += dirArr[i];
            }
        }

        private void InitializeWriteToFile(string dir, string fileName)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
                InitializeWriteToFile(dir, fileName);
                return;
            }

            WriteToCSV(dir, fileName);
        }

        private async void WriteToCSV(string dir, string fileName)
        {
            string fullPath = Path.Combine(dir, fileName);
            int chunkSizeDesired = 100;
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                using (var writer = new BinaryWriter(stream))
                {
                    _byteLeft = _csvHelper.CSV.Length;
                    _byteWritten = 0;

                    while (_byteLeft > 0)
                    {
                        var chunkSize = Mathf.Min(chunkSizeDesired, _byteLeft);
                        writer.Write(_csvHelper.CSV.ToCharArray(), _byteWritten, chunkSize);
                        _byteLeft -= chunkSize;
                        _byteWritten += chunkSize;
                        await UniTask.Delay(1);
                    }

                    _finishDownload = true;
                }
            }
        }

        private void AddValidNetFunctions(NetworkDataLog logFile, int index)
        {
            if (_spawnMapCount < _maxSpawnMapCount)
            {
                if (logFile.FunctionInfo[index].NetFuncName == GameConst.RE_SPAWN_MAP_STRING)
                {
                    _spawnMapCount = Mathf.Clamp(_spawnMapCount + 1, 0, _maxSpawnMapCount);
                    AddToCsvElements(logFile, index);
                }
            }

            if (_driverDeliveryCount < _maxDriverDeliveryCount)
            {
                if (logFile.FunctionInfo[index].NetFuncName == GameConst.RE_DRIVER_DELIVERY_STRING)
                {
                    _driverDeliveryCount = Mathf.Clamp(_driverDeliveryCount + 1, 0, _maxDriverDeliveryCount);
                    AddToCsvElements(logFile, index);
                }
            }

            if (_sendInputCount < _maxSendInputCount)
            {
                if (logFile.FunctionInfo[index].NetFuncName == GameConst.RE_LOCAL_SEND_INPUT_STRING)
                {
                    _sendInputCount = Mathf.Clamp(_sendInputCount + 1, 0, _maxSendInputCount);
                    AddToCsvElements(logFile, index);
                }
            }
        }

        private void AddToCsvElements(NetworkDataLog logFile, int index)
        {
            CsvElement elem = new CsvElement();
            elem.MatchId = logFile.RoomInfo.MatchId;
            elem.IpAddress = logFile.RoomInfo.IpAddress;
            elem.PlayerId = logFile.FunctionInfo[index].PlayerId;
            elem.FunctionName = logFile.FunctionInfo[index].NetFuncName;
            elem.CommType = logFile.FunctionInfo[index].CommunicationType;
            elem.DataSize = logFile.FunctionInfo[index].DataSize.ToString();
            elem.DatagramSize = logFile.FunctionInfo[index].DatagramSize.ToString();
            elem.Sent = logFile.FunctionInfo[index].TimeSent;
            elem.Received = logFile.FunctionInfo[index].TimeReceived;
            elem.TimeMiliseconds = logFile.FunctionInfo[index].DiffInMiliseconds.ToString();
            _csvElements.Add(elem);
            _contentLength++;
        }
    }
}