﻿using UnityEditor;
using MarkupAttributes.Editor;

namespace NPP.REIM.Configs
{
    [CustomEditor(typeof(ViewerConfigData))]
    public class ViewerConfigDataEditor : MarkedUpEditor { }
}