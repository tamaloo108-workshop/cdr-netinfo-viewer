﻿using UnityEditor;
using UnityEngine;

namespace NPP.REIM.Configs
{
    [JetBrains.Annotations.UsedImplicitly, CustomPropertyDrawer(typeof(PasswordFieldAttribute))]
    public class PasswordFieldDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.LabelField(position, label);
            EditorGUI.PasswordField(new Rect(position.x + EditorGUIUtility.labelWidth + 2f, position.y, position.width, position.height), property.stringValue);
        }
    }
}