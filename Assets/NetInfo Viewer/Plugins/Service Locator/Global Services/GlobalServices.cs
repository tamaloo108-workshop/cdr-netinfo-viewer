
using NPP.REIM.Configs;
using NPP.REIM.Services;
using NPP.REIM.Services.Generics;

public class GlobalServices
{
    private static ViewerConfigData _gameConfig;

    public static ViewerConfigData GameConfig
    {
        get
        {
            if (_gameConfig == null)
                _gameConfig = ServiceLocator.Current.Get<AssetLoader>().Load<ViewerConfigData>("CDR NetInfo Viewer", "Configs");

            return _gameConfig;
        }
    }

    public static float NormalizeValue(float values, float maxTarget)
    => (values - maxTarget * UnityEngine.Mathf.Floor((values + maxTarget) / maxTarget));
    public static float ConvertRatio(float oldValue, float oldMin, float oldMax, float newMin = 0f, float newMax = 1f)
    => (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;

    public static int GetStringSize(params string[] t)
    {
        int size = 0;

        if (t == null || t.Length == 0)
            return size;

        foreach (string item in t)
        {
            size += item.Length * sizeof(char);
        }

        return size;
    }

    public static int GetIntSize(params int[] i)
    {
        int size = 0;

        if (i == null || i.Length == 0)
            return size;

        foreach (int item in i)
        {
            size += sizeof(int);
        }

        return size;
    }

    public static int GetFloatSize(params float[] f)
    {
        int size = 0;

        if (f == null || f.Length == 0)
            return size;

        foreach (float item in f)
        {
            size += sizeof(float);
        }

        return size;
    }

    public static int GetBoolSize(params bool[] b)
    {
        int size = 0;

        if (b == null || b.Length == 0)
            return size;

        foreach (bool item in b)
        {
            size += sizeof(bool);
        }

        return size;
    }
}
