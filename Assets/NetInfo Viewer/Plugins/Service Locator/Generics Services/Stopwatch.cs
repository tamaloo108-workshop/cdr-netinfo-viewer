﻿using System.Runtime.InteropServices;

namespace NPP.REIM.Services.Generics
{
    public class Stopwatch : IServices
    {
        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

        [DllImport("Kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(out long lpFrequency);

        private long _freq;
        private long _start;
        private long _stop;

        public Stopwatch()
        {
            CalibrateFrequency();
        }

        public void CalibrateFrequency()
        {
            QueryPerformanceFrequency(out _freq);
        }

        public void Now(out long timestamp)
        {
            QueryPerformanceCounter(out timestamp);
        }

        public long Now()
        {
            QueryPerformanceCounter(out long timestamp);
            return timestamp;
        }

        public void Start()
        {
            QueryPerformanceCounter(out _start);
        }

        public void Stop()
        {
            QueryPerformanceCounter(out _stop);
        }

        public double Duration
        {
            get
            {
                return (double)(_stop - _start) / _freq;
            }
        }

        public double CalculateDuration(long start, long end)
        {
            return (double)(end - start) / _freq;
        }
    }
}