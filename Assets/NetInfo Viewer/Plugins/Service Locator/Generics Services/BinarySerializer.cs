﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace NPP.REIM.Services.Generics
{
    public interface ISerializable { }

    public class BinarySerializer : IServices
    {
        public void Serialize<T>(T data, string folder, string fileName) where T : ISerializable
        {
            string filePath = Path.Combine(Application.persistentDataPath, folder, $"{fileName}-save.stvr");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(filePath, FileMode.Create);

            bf.Serialize(fs, (T)data);
        }

        public T Deserialize<T>(string folder, string fileName) where T : ISerializable
        {
            string filePath = Path.Combine(Application.persistentDataPath, folder, $"{fileName}-save.stvr");

            if (File.Exists(filePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream(filePath, FileMode.Open);

                return (T)bf.Deserialize(fs);
            }

            return default(T);
        }
    }
}