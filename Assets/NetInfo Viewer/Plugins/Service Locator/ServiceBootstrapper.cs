﻿using NPP.REIM.DataLogging;
using NPP.REIM.Services.Generics;
using UnityEngine;

namespace NPP.REIM.Services
{
    public sealed class ServiceBootstrapper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            //register services.
            ServiceLocator.Current.Register(new JSONSerializer());
            ServiceLocator.Current.Register(new BinarySerializer());
            ServiceLocator.Current.Register(new SceneLoader());
            ServiceLocator.Current.Register(new AssetLoader());
            ServiceLocator.Current.Register(new WebFtpServices());

            ServiceLocator.Current.Get<SceneLoader>().LoadScene("Initialization");
        }
    }

}