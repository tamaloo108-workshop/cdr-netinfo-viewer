﻿using System.Collections.Generic;

namespace NPP.REIM.Services
{
    public sealed class ServiceLocator
    {
        private static readonly Dictionary<string, IServices> _services;
        private static ServiceLocator _current;

        static ServiceLocator()
        {
            _services = new Dictionary<string, IServices>();
        }

        public static ServiceLocator Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new ServiceLocator();
                }
                return _current;
            }
        }

        public void Register<T>(T services) where T : IServices
        {
            if (!_services.ContainsKey(typeof(T).Name))
            {
                _services.Add(typeof(T).Name, services);
            }
        }

        public T Get<T>() where T : IServices
        {
            if (_services.ContainsKey(typeof(T).Name))
            {
                return (T)_services[typeof(T).Name];
            }

            return default;
        }

        public void Unregister<T>() where T : IServices
        {
            if (_services.ContainsKey(typeof(T).Name))
            {
                _services.Remove(typeof(T).Name);
            }
        }
    }

}